import ContentHeader from '../../components/ContentHeader';
import HistoryFinanceCard from '../../components/HistoryFinanceCard';
import SelectInput from '../../components/SelectInput';
import { Container, Content } from './styles';

const List: React.FC = () => {
  const option = [{ value: 'Teste1', label: 'Teste', disabled: true }];
  return (
    <>
      <Container>
        <ContentHeader title="Saidas" lineColor="#e44c4e">
          <SelectInput options={option} />
        </ContentHeader>
        <Content>
          <HistoryFinanceCard
            tagColor="#e44c4e"
            title="Conta de Luz"
            subtitle="07/07/2023"
            amount="R$ 299,89"
          />
        </Content>
      </Container>
    </>
  );
};

export default List;
