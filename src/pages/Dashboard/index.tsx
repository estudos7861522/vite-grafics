import ContentHeader from '../../components/ContentHeader';
import SelectInput from '../../components/SelectInput';
import { Container } from './styles';

const Dashboard: React.FC = () => {
  const opMonth = [{ value: 'Month', label: 'Month' }];
  const opYear = [{ value: 'Year', label: 'Year' }];

  return (
    <>
      <Container>
        <ContentHeader title="Dasboard" lineColor="#f7931b">
          <SelectInput options={opMonth} />
          <SelectInput options={opYear} />
        </ContentHeader>
      </Container>
    </>
  );
};

export default Dashboard;
