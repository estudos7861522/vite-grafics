import Aside from '../Aside';
import Content from '../Content';
import Header from '../Header';
import { Grid } from './styles';

interface IProps {
  children: React.ReactNode;
}

const Layout: React.FC<IProps> = ({ children }) => {
  return (
    <>
      <Grid>
        <Header />
        <Aside />
        <Content>
          { children }
        </Content>
      </Grid>
    </>
  );
};

export default Layout;
