import styled from 'styled-components';


interface ITagProps {
  color: string;
}

export const Container = styled.li`
  background-color: ${props => props.theme.colors.tertiary};

  list-style: none;
  border-radius: 6px;
  margin: 15px 0;
  padding: 12px 10px;

  max-width: 95%;

  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
  transition: all .3s;

  position: relative;

  &:hover{
    opacity: .7;
    transform: translateX(10px);
  }
`;

export const Tag = styled.div<ITagProps>`
  background-color: ${(props) => props.color};

  position: absolute;
  left: 0;

  width: 10px;
  height: 60%;
`;

export const Card = styled.div`
   min-height: 38px;
   display: flex;
   flex-direction: column;


   justify-content: space-between;
   padding: 0 10px;
`;

export const Title = styled.h3`
   font-weight: 600;
`;

export const Subtitle = styled.p`
   font-size:.8em;
`;

export const Textvalue = styled.h3``;
