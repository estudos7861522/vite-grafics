import { Card, Container, Subtitle, Tag, Textvalue, Title } from './styles';

interface IHistoryFinanceCardProps{
  tagColor: string;
  title: string;
  subtitle: string;
  amount: string;
}

const HistoryFinanceCard: React.FC<IHistoryFinanceCardProps> = ({  
  tagColor, 
  title, 
  subtitle, 
  amount
}) => {
  return (
    <>
      <Container>
        <Tag color={tagColor} />
        <Card>
          <Title>
            {title}
          </Title>
          <Subtitle>
            {subtitle}
          </Subtitle>
        </Card>
        <Textvalue>
          {amount}
        </Textvalue>
      </Container>
    </>
  );
};

export default HistoryFinanceCard;