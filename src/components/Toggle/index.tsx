import { useState } from "react";
import { Container, ToggleLabel, ToggleSelector } from "./styles";


const Toggle: React.FC = () => {
  
  const [checkeds, setCheckeds]  = useState(false); 

  function handleCheckeds(): void{
    setCheckeds(!checkeds);
    console.log(checkeds);
  }

  return (
    <Container>
      <ToggleLabel>Light</ToggleLabel>
      <ToggleSelector 
         onChange={handleCheckeds} 
         checked={checkeds}  
         checkedIcon={false}
         uncheckedIcon={false}/>

      <ToggleLabel>Dark</ToggleLabel>
    </Container>
  );
};

export default Toggle;
