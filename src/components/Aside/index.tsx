import {
  Container,
  Header,
  LogImg,
  MenuContainer,
  MenuIconLink,
  Title,
} from './styles';

import {
  MdDashboard,
  MdArrowDownward,
  MdArrowUpward,
  MdExitToApp,
} from 'react-icons/md';

import logoImg from '../../assets/Logo.svg';

const Aside: React.FC = () => {
  return (
    <>
      <Container>
        <Header>
          <LogImg src={logoImg} />
          <Title>Minha Carteira</Title>
        </Header>
        <MenuContainer>
          <MenuIconLink href="#">
            <MdDashboard /> Dashboard
          </MenuIconLink>
          <MenuIconLink href="#">
            <MdArrowUpward /> Entradas
          </MenuIconLink>
          <MenuIconLink href="#">
            <MdArrowDownward /> Saida
          </MenuIconLink>
          <MenuIconLink href="#">
            <MdExitToApp /> Sair
          </MenuIconLink>
        </MenuContainer>
      </Container>
    </>
  );
};

export default Aside;
