import styled from 'styled-components';



export const Container = styled.div`
  grid-area: content;
  color: ${props => props.theme.colors.white};
  background-color:${props => props.theme.colors.primary};

  padding: 25px;

  height: calc(100vh - 120px); 
  overflow-y: scroll;

 
  
  



  ::-webkit-scrollbar{
    width: 20px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: ${props => props.theme.colors.secondary};
    border-radius: 10px;
    height: 20%;
    

    &:hover {
      background-color:  ${props => props.theme.colors.primary};
      border: 3px solid ${props => props.theme.colors.secondary};
    }
  }

  ::-webkit-scrollbar-track {
    background-color: ${props => props.theme.colors.tertiary};
    margin: 210px;
    border-radius: 10px;
    max-height: 50%;
    
  }

`;


